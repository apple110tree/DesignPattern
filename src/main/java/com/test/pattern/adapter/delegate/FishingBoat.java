package com.test.pattern.adapter.delegate;

public class FishingBoat {
	public void row() {
		System.out.println("FishingBoat start rowing");
	}
}
