package com.test.pattern.adapter.delegate;

public class RowingBoat {
	public void row() {
		System.out.println("RowingBoat start rowing");
	}
}
