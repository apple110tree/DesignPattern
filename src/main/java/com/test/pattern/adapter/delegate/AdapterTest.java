package com.test.pattern.adapter.delegate;

public class AdapterTest {
	public static void main(String[] args) {
		FishingBoatAdapter fishingBoatAdapter = new FishingBoatAdapter();
		
		Captain captain = new Captain(fishingBoatAdapter);
		captain.row();
	}
}
