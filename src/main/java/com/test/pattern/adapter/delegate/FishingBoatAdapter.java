package com.test.pattern.adapter.delegate;

public class FishingBoatAdapter extends RowingBoat{
	private FishingBoat fishingBoat;
	
	public FishingBoatAdapter() {
		// TODO Auto-generated constructor stub
		fishingBoat = new FishingBoat();
	}

	@Override
	public void row() {
		fishingBoat.row();
	}
}
