package com.test.pattern.adapter.delegate;

public class Captain {
	private RowingBoat rowingBoat;
	
	public Captain(RowingBoat rowingBoat) {
		// TODO Auto-generated constructor stub
		this.rowingBoat = rowingBoat;
	}

	public void row() {
		this.rowingBoat.row();
	}
}
