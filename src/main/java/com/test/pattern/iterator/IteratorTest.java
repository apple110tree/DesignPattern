package com.test.pattern.iterator;

import java.util.Iterator;

public class IteratorTest {
	public static void main(String[] args) {
		BookShelf bookShelf = new BookShelf();
		
		bookShelf.append(new Book("a book"));
		bookShelf.append(new Book("b book"));
		bookShelf.append(new Book("c book"));
		
		for(Iterator<Book> iter = bookShelf.iterator(); iter.hasNext();) {
			Book book = iter.next();
			System.out.println(book.getName());
		}
	}
}
