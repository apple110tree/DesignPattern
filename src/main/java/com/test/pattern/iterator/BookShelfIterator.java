package com.test.pattern.iterator;

import java.util.Iterator;

public class BookShelfIterator<T> implements Iterator<T>  {
	private BookShelf bookShelf;
	private int index;
	
	public BookShelfIterator(BookShelf bookShelf) {
		// TODO Auto-generated constructor stub
		this.bookShelf = bookShelf;
		this.index = 0;
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return this.index < bookShelf.getLength();
	}

	@SuppressWarnings("unchecked")
	@Override
	public T next() {
		// TODO Auto-generated method stub
		if(this.index < bookShelf.getLength()) {
			return (T) bookShelf.getBook(this.index++);
		}
		
		return null;
	}

}
