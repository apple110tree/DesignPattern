package com.test.pattern.iterator;

import java.util.Iterator;

public interface Aggregate {
	public <T> Iterator<T> iterator();
}
