package com.test.pattern.iterator;

import java.util.ArrayList;
import java.util.Iterator;

public class BookShelf implements Aggregate {
	private ArrayList<Book> books;
	
	public BookShelf() {
		// TODO Auto-generated constructor stub
		books = new ArrayList<>();
	}
	
	public BookShelf(int size) {
		// TODO Auto-generated constructor stub
		books = new ArrayList<>(size);
	}
	
	public void append(Book book) {
		books.add(book);
	}
	
	public Book getBook(int index) {
		return books.get(index);
	}
	
	public int getLength() {
		return books.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterator<Book> iterator() {
		return new BookShelfIterator<Book>(this);
	}
	
}
