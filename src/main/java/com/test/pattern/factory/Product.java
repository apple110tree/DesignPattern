package com.test.pattern.factory;

public interface Product {
	public void use();
}
