package com.test.pattern.factory;

public class Phone implements Product {
	private String number;
	
	public Phone(String number) {
		// TODO Auto-generated constructor stub
		this.number = number;
	}
	
	@Override
	public void use() {
		System.out.println("call "+this.number+" ....");
	}
}
