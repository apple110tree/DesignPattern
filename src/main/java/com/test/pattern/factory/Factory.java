package com.test.pattern.factory;

public interface Factory {
	public Product createProduct(String number);
}
