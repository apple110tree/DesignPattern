package com.test.pattern.factory;

public class FactoryTest {
	public static void main(String[] args) {
		Factory factory = new PhoneFactory();
		Product phone1 = factory.createProduct("12344444");
		Product phone2 = factory.createProduct("3333232323");
		
		phone1.use();
		phone2.use();
	}
}
